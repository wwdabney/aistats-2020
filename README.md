## AI Stats 2020 - Virtual Conference - August 26 - 28 

We are reaching the end of AISTATS 2020. We hope you had an enjoyable time and were able to find engaging conversation with paper authors, invited speakers, and other attendees.

This year brought a dramatic increase in paper submissions, and we wish to highlight those papers that were found especially noteworthy with the following awards.

### Notable paper awards:

* [**Regularity as Regularization: Smooth and Strongly Convex Brenier Potentials in Optimal Transport**](http://proceedings.mlr.press/v108/paty20a.html).<br> François-Pierre Paty,  Alexandre d’Aspremont,  Marco Cuturi. ([Presentation](https://aistats2020.net/poster_457.html))

* [**On Thompson Sampling for Smoother-than-Lipschitz Bandits**](http://proceedings.mlr.press/v108/grant20a.html).<br> James Grant,  David Leslie. ([Presentation](https://aistats2020.net/poster_856.html))

* [**Variational Optimization on Lie Groups, with Examples of Leading (Generalized) Eigenvalue Problems**](http://proceedings.mlr.press/v108/tao20a.html).<br> Molei Tao, Tomoki Ohsawa. ([Presentation](https://aistats2020.net/poster_1465.html))


Congratulations to the authors!

Finally, we want to express our thanks to our invited speakers, sponsors, and all participants for making AISTATS 2020 memorable.


