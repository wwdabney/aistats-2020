#python parse_calendar.py data/qa_schedule.tsv > sitedata/main_calendar.json

import csv
import sys
from collections import namedtuple
import subprocess
import datetime
from dateutil.parser import parse
import pytz
import json

qa_sched_fname = sys.argv[1]

# Process QA schedule data
sched_data = subprocess.check_output(
        ["awk", 'BEGIN{FS="\t";} {print $1"\t"$3"\t"$6;}', qa_sched_fname])
sched_data = sched_data.strip(" ").strip("\n")
sched_data = sched_data.split("\n")[1:] # Remove header
sched_data = [d.strip().split("\t") for d in sched_data if len(d.strip()) > 0]

day = None
timezone = pytz.timezone('UTC')
all_data = '['

# Daphne's talk
event = dict(calendarId='qa', category='time',
             start=datetime.datetime(year=2020, month=8, day=26, hour=23-2, minute=0, tzinfo=pytz.timezone('UTC')).astimezone(timezone).isoformat(),
             end=datetime.datetime(year=2020, month=8, day=26, hour=24-2, minute=0, tzinfo=pytz.timezone('UTC')).astimezone(timezone).isoformat(),
             title='Live Q&A with Daphne Koller (Machine Learning: A New Approach to Drug Discovery)',
             location='speaker_2.html')
all_data += json.dumps(event) + ','

event = dict(calendarId='qa', category='time',
             start=datetime.datetime(year=2020, month=8, day=27, hour=20-2, minute=0, tzinfo=pytz.timezone('UTC')).astimezone(timezone).isoformat(),
             end=datetime.datetime(year=2020, month=8, day=27, hour=21-2, minute=0, tzinfo=pytz.timezone('UTC')).astimezone(timezone).isoformat(),
             title='Live Q&A with Daphne Koller (Machine Learning: A New Approach to Drug Discovery)',
             location='speaker_2.html')
all_data += json.dumps(event) + ','

# Michael's talk
event = dict(calendarId='qa', category='time',
             start=datetime.datetime(year=2020, month=8, day=26, hour=20-2, minute=0, tzinfo=pytz.timezone('UTC')).astimezone(timezone).isoformat(),
             end=datetime.datetime(year=2020, month=8, day=26, hour=21-2, minute=0, tzinfo=pytz.timezone('UTC')).astimezone(timezone).isoformat(),
             title='Live Q&A with Michael I. Jordan (Towards a Blend of Machine Learning and Microeconomics)',
             location='speaker_1.html')
all_data += json.dumps(event) + ','

event = dict(calendarId='qa', category='time',
             start=datetime.datetime(year=2020, month=8, day=28, hour=13-2, minute=0, tzinfo=pytz.timezone('UTC')).astimezone(timezone).isoformat(),
             end=datetime.datetime(year=2020, month=8, day=28, hour=14-2, minute=0, tzinfo=pytz.timezone('UTC')).astimezone(timezone).isoformat(),
             title='Live Q&A with Michael I. Jordan (Towards a Blend of Machine Learning and Microeconomics)',
             location='speaker_1.html')
all_data += json.dumps(event) + ','

# Mentoring sessions
mentoring_times = [(26, 21), (26, 22),
                   (27, 5), (27, 6), (27, 14), (27, 15), (27, 21),
                   (28, 8), (28, 14), (28, 15)]
for m, ment in enumerate(mentoring_times):
    event = dict(calendarId='mentoring', category='time',
                 start=datetime.datetime(year=2020, month=8, day=ment[0], hour=ment[1]-2, minute=0, tzinfo=pytz.timezone('UTC')).astimezone(timezone).isoformat(),
                 end=datetime.datetime(year=2020, month=8, day=ment[0], hour=ment[1]+1-2, minute=0, tzinfo=pytz.timezone('UTC')).astimezone(timezone).isoformat(),
                 title='Mentoring Session %d' % (m+1),
                 location='mentors.html?session=' + str(m+1))
    all_data += json.dumps(event) + ','

for row in sched_data:
    if len(row) == 3: # Changing the date
        day = row[0]
        row = row[1:]

    start, end = row[1].split("-")
    dt_str = day + " " + start + " UTC"
    start_time = parse(dt_str) - datetime.timedelta(hours=2)
    dt_str = day + " " + end + " UTC"
    end_time = parse(dt_str) - datetime.timedelta(hours=2)
    link = "papers.html?session=" + row[0] + "&filter=keywords"

    #title = "Poster Day %s Session %s (%s)" % (row[0][1], row[0][3], row[0])
    title = "Live Paper Q&A %s" % row[0][2:]
    event = dict(calendarId='---', category='time',
                 start=start_time.astimezone(timezone).isoformat(),
                 end=end_time.astimezone(timezone).isoformat(),
                 title=title,
                 link=link,
                 location=link)
    all_data += json.dumps(event) + ','

# TODO: Add invited talks
all_data = all_data[:-1] + ']'
print(all_data)



#def event_to_json(start, end, title, session):
    

