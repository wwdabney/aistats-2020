# python parse_papers.py data/camera_ready_list.tsv data/slideslive_links_v2.tsv data/qa_sched.tsv > sitedata/papers.csv

import csv
import sys
from collections import namedtuple
import subprocess
import pytz
import datetime
from dateutil.parser import parse

Paper = namedtuple("Paper", ['id', 'title', 'abstract', 'names', 'emails',
                             'contact_email', 'track', 'files', 'submission_code', 
                             'copyright_form', 'pdf'])

fname_input = sys.argv[1]
slideslive_fname = sys.argv[2]
qa_sched_fname = sys.argv[3]

aistats_base = "http://proceedings.mlr.press/v108/" #sakaue20a/sakaue20a.pdf

### Process QA schedule data

# First, create a map from sessions (e.g. D2S3) to DateTime
sched_data = subprocess.check_output(
        ["awk", 'BEGIN{FS="\t";} {print $1"\t"$3"\t"$6;}', qa_sched_fname])
sched_data = sched_data.strip(" ").strip("\n")
sched_data = sched_data.split("\n")[1:] # Remove header
sched_data = [d.strip().split("\t") for d in sched_data if len(d.strip()) > 0]

day = None
timezone = pytz.timezone('UTC')
session_times = {}

for row in sched_data:
    if len(row) == 3: # Changing the date
        day = row[0]
        row = row[1:]

    start, end = row[1].split("-")
    dt_str = day + " " + start + " UTC"
    start_time = parse(dt_str) - datetime.timedelta(hours=2)
    dt_str = day + " " + end + " UTC"
    end_time = parse(dt_str) - datetime.timedelta(hours=2)
    #session_times[row[0]] = start_time.astimezone(timezone).strftime('%a %b %d, %H:%M') + '-' + end_time.astimezone(timezone).strftime('%H:%M') + ' UTC'
    session_times[row[0]] = start_time.astimezone(timezone).strftime('%d %b %Y - %H:%M:%S') + '-' + end_time.astimezone(timezone).strftime('%H:%M:%S')

#print(session_times)

# Next create a map from paper IDs to sessions
sched_data = subprocess.check_output(
        ["awk", 'BEGIN{FS="\t";} {printf("%s\t", $3); for(k=8;k<=NF;k++) printf("%s\t", $k); print "";}', qa_sched_fname])
sched_data = sched_data.strip(" ").strip("\n")
sched_data = sched_data.split("\n")[1:] # Remove header
sched_data = [d.strip().split("\t") for d in sched_data if len(d.strip()) > 0]
sched_dict = {}

for row in sched_data:
    for paper_id in row[1:]:
        sched_dict.setdefault(paper_id, []).append(row[0])

# Process slides live data
slides_data = subprocess.check_output(
        ["awk", 'BEGIN{FS="\t";} {print $1"\t"$6"\t"$2;}', slideslive_fname])
slides_data = slides_data.strip(" ").strip("\n")
slides_data = slides_data.split("\n")[1:] # Remove header
slides_dict = dict([(slide.split("\t")[0], slide.split("\t")[1:]) for slide in slides_data])

with open(fname_input, "r") as fin:
    reader = csv.reader(fin, delimiter='\t')
    headers = next(reader)
    print("UID,title,authors,abstract,keywords,session,pdf_url,slides_id,slide_status,times")
    for row in reader:
        paper = Paper(*row)
        pdf_url = aistats_base + paper.pdf.split(".")[0] + "/" + paper.pdf
        sessions = sched_dict[paper.id]
        sessions = [session_times[session] for session in sessions]
        outformat = [paper.id, '"' + paper.title.replace('"', "'") + '"', 
                     '"' + "|".join([name.strip().replace('"', "'") for name in paper.names.split(";")]) + '"',
                     '"' + paper.abstract.replace('"', "'") + '"', paper.track, # track is in place of keywords
                     '|'.join(sched_dict[paper.id]), pdf_url, 
                     slides_dict[paper.id][0].split("/")[-1],slides_dict[paper.id][1],
                     '"' + '; '.join(sessions) + '"']
        print(",".join(outformat))




